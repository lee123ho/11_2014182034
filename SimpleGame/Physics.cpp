#include "stdafx.h"
#include "Physics.h"

Physics::Physics() {

}

Physics::~Physics() {

}

bool Physics::isOverlap(Object* a, Object* b, int method) {
	switch (method)
	{
	case 0:	// BB Overlap Test
		return BBOverlapTest(a, b);
		break;
	default:
		break;
	}

	return false;
}

bool Physics::BBOverlapTest(Object* a, Object* b) {
	// A object
	float aX, aY, aZ;
	float aSizeX, aSizeY, aSizeZ;
	float aMinX, aMinY, aMinZ;
	float aMaxX, aMaxY, aMaxZ;
	a->GetPos(&aX, &aY, &aZ);
	a->GetVol(&aSizeX, &aSizeY, &aSizeZ);
	aMinX = aX - aSizeX / 2.f; aMaxX = aX + aSizeX / 2.f;
	aMinY = aY - aSizeY / 2.f; aMaxY = aY + aSizeY / 2.f;
	aMinZ = aZ - aSizeZ / 2.f; aMaxZ = aZ + aSizeZ / 2.f;
	
	// B object
	float bX, bY, bZ;
	float bSizeX, bSizeY, bSizeZ;
	float bMinX, bMinY, bMinZ;
	float bMaxX, bMaxY, bMaxZ;
	b->GetPos(&bX, &bY, &bZ);
	b->GetVol(&bSizeX, &bSizeY, &bSizeZ);
	bMinX = bX - bSizeX / 2.f; bMaxX = bX + bSizeX / 2.f;
	bMinY = bY - bSizeY / 2.f; bMaxY = bY + bSizeY / 2.f;
	bMinZ = bZ - bSizeZ / 2.f; bMaxZ = bZ + bSizeZ / 2.f;

	if (aMinX > bMaxX)
		return false;
	if (aMaxX < bMinX)
		return false;

	if (aMinY > bMaxY)
		return false;
	if (aMaxY < bMinY)
		return false;

	if (aMinZ > bMaxZ)
		return false;
	if (aMaxZ < bMinZ)
		return false;

	return true;
}

void Physics::ProcessCollision(Object* a, Object* b) {
	// A object
	float aM, aVelX, aVelY, aVelZ;
	a->GetMass(&aM);
	a->GetVel(&aVelX, &aVelY, &aVelZ);
	float afVelX, afVelY, afVelZ;

	// B object
	float bM, bVelX, bVelY, bVelZ;
	b->GetMass(&bM);
	b->GetVel(&bVelX, &bVelY, &bVelZ);
	float bfVelX, bfVelY, bfVelZ;

	afVelX = ((aM - bM) / (aM + bM)) * aVelX + ((2.f * bM) / (aM + bM)) * bVelX;
	afVelY = ((aM - bM) / (aM + bM)) * aVelY + ((2.f * bM) / (aM + bM)) * bVelY;
	afVelZ = ((aM - bM) / (aM + bM)) * aVelZ + ((2.f * bM) / (aM + bM)) * bVelZ;

	bfVelX = ((2.f * aM) / (aM + bM)) * aVelX + ((bM - aM) / (aM + bM)) * bVelX;
	bfVelY = ((2.f * aM) / (aM + bM)) * aVelY + ((bM - aM) / (aM + bM)) * bVelY;
	bfVelZ = ((2.f * aM) / (aM + bM)) * aVelZ + ((bM - aM) / (aM + bM)) * bVelZ;

	a->SetVel(afVelX, afVelY, afVelZ);
	b->SetVel(bfVelX, bfVelY, bfVelZ);
}

