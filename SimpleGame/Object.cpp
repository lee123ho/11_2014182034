#include "stdafx.h"
#include "Object.h"
#include "Globals.h"
#include <math.h>
#include <float.h>

Object::Object()
{
	Initphysics();
}

Object::~Object()
{

}

void Object::Initphysics()
{
	m_r = m_g = m_b = m_a = 0.f;
	m_mass = 0.f;
	m_posX = m_posY = m_posZ = 0.f;
	m_velX = m_velY = m_velZ = 0.f;
	m_accX = m_accY = m_accZ = 0.f;
	m_volX = m_volY = m_volZ = 0.f;
	m_fricCoef = 0.f;
}

bool Object::CanShootBullet()
{
	if (m_remainingCoolTime < FLT_EPSILON)
		return true;
	else
		return false;
}

void Object::ResetBulletCoolTime()
{
	m_remainingCoolTime = m_CurrentCoolTime;
}

void Object::Update(float elapsedTime)
{
	// reduce remaining cooltime
	m_remainingCoolTime -= elapsedTime;

	// 마찰력
	float nForce = m_mass * Gravity;	// scalar(크기를 의미함, 수직 항력의 size)
	float fForce = m_fricCoef * nForce;	// scalar
	float velSize = sqrtf(m_velX * m_velX + m_velY * m_velY + m_velZ*m_velZ);
	if (velSize > 0.f) {
		float fDirX = -1.f * m_velX / velSize;
		float fDirY = -1.f * m_velY / velSize;
		fDirX *= fForce;
		fDirY *= fForce;

		float fAccX = fDirX / m_mass;
		float fAccY = fDirY / m_mass;

		float newX = m_velX + fAccX * elapsedTime;
		float newY = m_velY + fAccY * elapsedTime;
		if (newX * m_velX < 0.f)
			m_velX = 0.f;
		else
			m_velX = newX;

		if (newY * m_velY < 0.f)
			m_velY = 0.f;
		else
			m_velY = newY;
		m_velZ = m_velZ - Gravity * elapsedTime;
	}

	// 속력
	m_posX = m_posX + m_velX * elapsedTime;
	m_posY = m_posY + m_velY * elapsedTime;
	m_posZ = m_posZ + m_velZ * elapsedTime;
	
	if (m_posZ < FLT_EPSILON) {
		m_posZ = 0.f;
		m_velZ = 0.f;
	}

	age += elapsedTime;
}

void Object::AddForce(float x, float y, float z, float elapsedTime) {
	float accX, accY, accZ;
	accX = accY = accZ = 0.f;

	accX = x / m_mass;
	accY = y / m_mass;
	accZ = z / m_mass;

	m_velX += accX * elapsedTime;
	m_velY += accY * elapsedTime;
	m_velZ += accZ * elapsedTime;
}

void Object::SetVol(float x, float y, float z)
{
	m_volX = x;
	m_volY = y;
	m_volZ = z;
}
void Object::GetVol(float* x, float* y, float* z)
{
	*x = m_volX;
	*y = m_volY;
	*z = m_volZ;
}
void Object::SetPos(float x, float y, float z)
{
	m_posX = x;
	m_posY = y;
	m_posZ = z;
}
void Object::GetPos(float* x, float* y, float* z)
{
	*x = m_posX;
	*y = m_posY;
	*z = m_posZ;
}
void Object::SetColor(float r, float g, float b, float a)
{
	m_r = r;
	m_g = g;
	m_b = b;
	m_a = a;
}
void Object::GetColor(float* r, float* g, float* b, float* a)
{
	*r = m_r;
	*g = m_g;
	*b = m_b;
	*a = m_a;
}
void Object::SetVel(float x, float y, float z)
{
	m_velX = x;
	m_velY = y;
	m_velZ = z;
}
void Object::GetVel(float* x, float* y, float* z)
{
	*x = m_velX;
	*y = m_velY;
	*z = m_velZ;
}
void Object::SetAcc(float x, float y, float z)
{
	m_accX = x;
	m_accY = y;
	m_accZ = z;
}
void Object::GetAcc(float* x, float* y, float* z)
{
	*x = m_accX;
	*y = m_accY;
	*z = m_accZ;
}
void Object::SetMass(float x)
{
	m_mass = x;
}
void Object::GetMass(float* x)
{
	*x = m_mass;
}

void Object::SetType(int type)
{
	m_type = type;
}

void Object::GetType(int* type)
{
	*type = m_type;
}

void Object::SetFricCoef(float coef)
{
	m_fricCoef = coef;
}

void Object::GetFricCoef(float* coef)
{
	*coef = m_fricCoef;
}

void Object::SetTexID(int id)
{
	m_texID = id;
}

void Object::GetTexID(int* id)
{
	*id = m_texID;
}

void Object::SetParentObj(Object* parent) {
	m_Parent = parent;
}

Object* Object::GetParentObj() {
	return m_Parent;
}

bool Object::isAncestor(Object* obj) {
	if (obj != NULL) {
		if (obj == m_Parent) {
			return true;
		}
	}
	return false;
}

void Object::GetHp(float * hp) {
	*hp = HealthPoint;
}

void Object::SetHp(float hp) {
	HealthPoint = hp;
}

void Object::Damage(float damage) {
	HealthPoint -= damage;
}

void Object::GetAge(float* a)
{
	*a = age;
}
