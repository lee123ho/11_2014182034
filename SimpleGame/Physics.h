#include "Object.h"
#pragma once

class Physics
{
public:
	Physics();
	~Physics();

	bool isOverlap(Object* a, Object* b, int method = 0);
	void ProcessCollision(Object* a, Object* b);

private:
	bool BBOverlapTest(Object* a, Object* b);
};

