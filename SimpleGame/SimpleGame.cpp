/*
Copyright 2017 Lee Taek Hee (Korea Polytech University)

This program is free software: you can redistribute it and/or modify
it under the terms of the What The Hell License. Do it plz.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY.
*/

#include "stdafx.h"
#include <iostream>
#include "Dependencies\glew.h"
#include "Dependencies\freeglut.h"

#include "ScnMgr.h"

ScnMgr* g_ScnMgr = NULL;
int g_PrevTime = 0;

void RenderScene(int temp) {
	int CurrTime = glutGet(GLUT_ELAPSED_TIME);
	int ElapsedTime = CurrTime - g_PrevTime;
	float ElapsedTimeInSec = (float)ElapsedTime / 1000.f;
	g_PrevTime = CurrTime;

	g_ScnMgr->Update(ElapsedTimeInSec);
	g_ScnMgr->RenderScene();
	g_ScnMgr->DoGarbageCollection();

	// std::cout << "ElapsedTime : " << ElapsedTime << std::endl;

	glutSwapBuffers();

	glutTimerFunc(10, RenderScene, 0);
}

void Display() {

}

void Idle(void)
{
	
}

void MouseInput(int button, int state, int x, int y)
{
	
}

void KeyDownInput(unsigned char key, int x, int y)
{
	g_ScnMgr->KeyDownInput(key, x, y);
}

void KeyUpInput(unsigned char key, int x, int y)
{
	g_ScnMgr->KeyUpInput(key, x, y);
}

void SpecialKeyDownInput(int key, int x, int y)
{
	g_ScnMgr->SpecialKeyDownInput(key, x, y);
}

void SpecialKeyUpInput(int key, int x, int y)
{
	g_ScnMgr->SpecialKeyUpInput(key, x, y);
}

int main(int argc, char** argv)
{
	// Initialize GL things
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowPosition(0, 0);
	glutInitWindowSize(1000, 1000);
	glutCreateWindow("Game Software Engineering KPU");

	glewInit();
	if (glewIsSupported("GL_VERSION_3_0"))
	{
		std::cout << " GLEW Version is 3.0\n ";
	}
	else
	{
		std::cout << "GLEW 3.0 not supported\n ";
	}

	g_ScnMgr = new ScnMgr;

	glutDisplayFunc(Display);
	glutIdleFunc(Idle);
	glutKeyboardFunc(KeyDownInput);	// KeyDown Event Callback
	glutKeyboardUpFunc(KeyUpInput);	// KeyUp Event Callback
	glutMouseFunc(MouseInput);
	glutSpecialFunc(SpecialKeyDownInput);
	glutSpecialUpFunc(SpecialKeyUpInput);

	g_PrevTime = glutGet(GLUT_ELAPSED_TIME);
	// 타이머를 걸어 타이머마다 강제로 호출을 하게 만들어주는 함수
	glutTimerFunc(10, RenderScene, 0);

	glutMainLoop();

	delete g_ScnMgr;
	g_ScnMgr = NULL;

	return 0;
}

