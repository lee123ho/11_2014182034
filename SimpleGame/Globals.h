#pragma once

#define MAX_OBJECTS 1000

#define HERO_ID 0

#define Gravity 9.8f

#define TYPE_NORMAL 0
#define TYPE_ENEMY 1
#define TYPE_BULLET 2
#define TYPE_WALL 3