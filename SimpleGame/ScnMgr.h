#pragma once
#include "Renderer.h"
#include "Object.h"
#include "Globals.h"
#include "Physics.h"
#include "Sound.h"

class ScnMgr
{
public:
	ScnMgr();
	~ScnMgr();

	void Update(float elapsedTimeInSec);
	void RenderScene(void);
	void DoGarbageCollection();

	int AddObject(float x, float y, float z, 
		float sx, float sy, float sz, 
		float r, float g, float b, float a,
		float vx, float vy, float vz,
		float mass,
		float fricCoef,
		int type,
		float hp);

	void DeleteObject(int idx);

	void KeyDownInput(unsigned char key, int x, int y);
	void KeyUpInput(unsigned char key, int x, int y);

	void SpecialKeyDownInput(int key, int x, int y);
	void SpecialKeyUpInput(int key, int x, int y);

	void useItem();

private:
	Renderer* m_Renderer = NULL;
	Sound* m_Sound = NULL;
	Physics* m_Physics = NULL;
	Object* m_TestObject = NULL;
	Object* m_Object[MAX_OBJECTS];
	int m_TestIdx = -1;
	int m_TestIdxArray[MAX_OBJECTS];
	float rTime = 0.f;
	float re = 1.f;

	int count = 0;

	bool m_KeyW = false;
	bool m_KeyS = false;
	bool m_KeyA = false;
	bool m_KeyD = false;
	bool m_KeySP = false;
	bool m_KeyItem = false;
	bool m_KeyStart = false;

	bool m_KeyUp = false;
	bool m_KeyDown = false;
	bool m_KeyLeft = false;
	bool m_KeyRight = false;

	bool end = false;
	bool fail = false;
	int endSong = 0;
};

