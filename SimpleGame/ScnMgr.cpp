#include "stdafx.h"
#include "ScnMgr.h"
#include "Dependencies\freeglut.h"
#include <stdio.h>
#include <time.h>

int g_testTex;
int g_testAnimTex;
int g_testBGTex;
int g_eneTex;
int g_startTex;
int g_endTex;
int g_dieTex;

int g_BGM = -1;
int g_FIRE = -1;
int g_EXPL = -1;
int g_End = -1;
int g_DieEnd = -1;

int g_Particle = -1;

static bool fireSound = false;

ScnMgr::ScnMgr()
{
	// Initialize Renderer
	m_Renderer = new Renderer(1000, 1000);
	if (!m_Renderer->IsInitialized())
	{
		std::cout << "Renderer could not be initialized.. \n";
	}

	// init Physics
	m_Physics = new Physics();

	// init Sound
	m_Sound = new Sound();

	// Init Objects
	for (int i = 0; i < MAX_OBJECTS; ++i) {
		m_Object[i] = NULL;
	}

	// Create Hero
	m_Object[HERO_ID] = new Object();
	m_Object[HERO_ID]->SetColor(1, 1, 1, 1);
	m_Object[HERO_ID]->SetPos(0, 0, 0);
	m_Object[HERO_ID]->SetVol(0.5f, 0.5f, 0.5f);
	m_Object[HERO_ID]->SetVel(0, 0, 0);
	m_Object[HERO_ID]->SetMass(1.f);
	m_Object[HERO_ID]->SetFricCoef(0.7f);
	m_Object[HERO_ID]->SetHp(100);
	m_Object[HERO_ID]->SetType(TYPE_NORMAL);


	g_testTex = m_Renderer->GenPngTexture("./textures/patrickstar.png");
	g_startTex = m_Renderer->GenPngTexture("./textures/start.png");
	g_testBGTex = m_Renderer->GenPngTexture("./textures/bg.png");
	g_Particle = m_Renderer->GenPngTexture("./textures/particle.png");
	g_eneTex = m_Renderer->GenPngTexture("./textures/jelly.png");
	g_endTex = m_Renderer->GenPngTexture("./textures/end.png");
	g_dieTex = m_Renderer->GenPngTexture("./textures/die.png");
	m_Object[HERO_ID]->SetTexID(g_testTex);

	int temp = AddObject(
		-1.f, 1.f, 0.f,
		0.5f, 0.5f, 0.5f,
		1, 1, 1, 1,
		0, 0, 0,
		10,
		0.6,
		TYPE_ENEMY,
		20
	);

	//m_Object[temp]->SetTexID(g_eneTex);

	temp = AddObject(
		1.f, -1.f, 0.f,
		0.5f, 0.5f, 0.5f,
		1, 1, 1, 1,
		0, 0, 0,
		10,
		0.6,
		TYPE_ENEMY,
		20
	);

	//m_Object[temp]->SetTexID(g_eneTex);

	g_BGM = m_Sound->CreateBGSound("./sounds/bgm.mp3");
	m_Sound->PlayBGSound(g_BGM, true, 0.35f);
	g_FIRE = m_Sound->CreateShortSound("./sounds/shot.mp3");
	g_EXPL = m_Sound->CreateShortSound("./sounds/die.mp3");
	g_End = m_Sound->CreateShortSound("./sounds/loveYou.mp3");
	g_DieEnd = m_Sound->CreateShortSound("./sounds/herodie.mp3");

	g_Particle = m_Renderer->CreateParticleObject(
		10,
		-50, -50,
		50, 50,
		5.f, 5.f,
		10.f, 10.f,
		-50, -50,
		50, 50
	);
}

ScnMgr::~ScnMgr()
{
	if (m_Renderer != NULL) {
		delete m_Renderer;
		m_Renderer = NULL;
	}

	if (m_Physics != NULL) {
		delete m_Physics;
		m_Physics = NULL;
	}
}

void ScnMgr::Update(float elapsedTimeInSec) {
	if (fail) {
		end = true;
		m_Sound->StopBGSound(g_BGM);
		if (endSong == 0)
			m_Sound->PlayShortSound(g_DieEnd, false, 1.f);
		endSong++;
	}
	if (m_KeyStart) {
		if (m_Object[HERO_ID] != NULL) {
			// Character Control
			//std::cout << "w : " << m_KeyW << ", S : " << m_KeyS << ", A : " << m_KeyA << ", D : " << m_KeyD << std::endl;
			//std::cout << "Up : " << m_KeyUp << ", Down : " << m_KeyDown << ", Left : " << m_KeyLeft << ", Right : " << m_KeyRight << std::endl;
			float fx = 0.f, fy = 0.f, fz = 0.f;
			float fAmount = 15.f;

			if (m_KeyW == true) {
				fy += 1.f;
			}
			if (m_KeyS == true) {
				fy -= 1.f;
			}
			if (m_KeyA == true) {
				fx -= 1.f;
			}
			if (m_KeyD == true) {
				fx += 1.f;
			}
			if (m_KeySP == true) {
				fz += 1.f;
			}
			if (m_KeyItem == true) {

			}

			// Add Control Force To Hero
			float fsize = sqrtf(fx * fx + fy * fy);
			if (fsize > FLT_EPSILON) {
				fx /= fsize;
				fy /= fsize;
				fx *= fAmount;
				fy *= fAmount;

				m_Object[HERO_ID]->AddForce(fx, fy, 0.f, elapsedTimeInSec);
			}

			if (fz > FLT_EPSILON) {
				float x, y, z;
				m_Object[HERO_ID]->GetPos(&x, &y, &z);

				if (z < FLT_EPSILON) {
					fz *= fAmount * 15.f;
					m_Object[HERO_ID]->AddForce(0.f, 0.f, fz, elapsedTimeInSec);
				}
			}

			// Fire Bullets
			if (m_Object[HERO_ID]->CanShootBullet()) {
				float bulletVel = 7.f;
				float vBulletX, vBulletY, vBulletZ;
				vBulletX = vBulletY = vBulletZ = 0.f;
				if (m_KeyUp) vBulletY += 1.f;
				if (m_KeyDown) vBulletY -= 1.f;
				if (m_KeyLeft) vBulletX -= 1.f;
				if (m_KeyRight) vBulletX += 1.f;

				float vBulletSize = sqrtf(vBulletX * vBulletX + vBulletY * vBulletY + vBulletZ * vBulletZ);
				if (vBulletSize > FLT_EPSILON) {
					// Create Bullet Object
					vBulletX /= vBulletSize;
					vBulletY /= vBulletSize;
					vBulletZ /= vBulletSize;

					vBulletX *= bulletVel;
					vBulletY *= bulletVel;
					vBulletZ *= bulletVel;

					float hx, hy, hz;
					float hvx, hvy, hvz;
					m_Object[HERO_ID]->GetPos(&hx, &hy, &hz);
					m_Object[HERO_ID]->GetVel(&hvx, &hvy, &hvz);

					vBulletX += hvx;
					vBulletY += hvy;
					vBulletZ += hvz;

					int id = AddObject(hx, hy, hz,
						0.05f, 0.05f, 0.05f,
						1, 0, 0, 1,
						vBulletX, vBulletY, vBulletZ,
						1.f,
						0.7f,
						TYPE_BULLET,
						5);

					//m_Object[id]->AddForce(vBulletX, vBulletY, vBulletZ, 0.1);
					m_Object[id]->SetParentObj(m_Object[HERO_ID]);
					m_Object[HERO_ID]->ResetBulletCoolTime();
					m_Sound->PlayShortSound(g_FIRE, false, 0.35f);
				}
			}

			// Overlap Test
			for (int src = 0; src < MAX_OBJECTS; src++) {
				for (int dst = src + 1; dst < MAX_OBJECTS; dst++) {											// 자기 자신과는 충돌하지 않기 위해 src+1로 추가함
					if (m_Object[src] != NULL && m_Object[dst] != NULL) {
						if (m_Physics->isOverlap(m_Object[src], m_Object[dst])) {
							printf("Collision : %d, %d\n", src, dst);
							int a, b;
							m_Object[src]->GetType(&a);
							m_Object[dst]->GetType(&b);
							if (!m_Object[src]->isAncestor(m_Object[dst]) && !m_Object[dst]->isAncestor(m_Object[src]) && a != b) {
								// Process Collision
								m_Physics->ProcessCollision(m_Object[src], m_Object[dst]);

								// Damage
								float srcHp, dstHp;
								m_Object[src]->GetHp(&srcHp);
								m_Object[dst]->GetHp(&dstHp);
								m_Object[src]->Damage(dstHp);
								m_Object[dst]->Damage(srcHp);

								m_Sound->PlayShortSound(g_EXPL, false, 1.f);
							}
						}
					}
				}
			}

			for (int i = 0; i < MAX_OBJECTS; i++) {
				if (m_Object[i] != NULL) {
					float x, y, z;
					m_Object[i]->GetPos(&x, &y, &z);
					if (x > 4.5f) {
						x = 4.5;
						m_Object[i]->SetPos(x, y, z);
						m_Object[i]->SetVel(0, 0, 0);
					}
					if (x < -4.5f) {
						x = -4.5;
						m_Object[i]->SetPos(x, y, z);
						m_Object[i]->SetVel(0, 0, 0);
					}
					if (y > 4.f) {
						y = 4.f;
						m_Object[i]->SetPos(x, y, z);
						m_Object[i]->SetVel(0, 0, 0);
					}
					if (y < -4.f) {
						y = -4.f;
						m_Object[i]->SetPos(x, y, z);
						m_Object[i]->SetVel(0, 0, 0);
					}
				}
			}

			if (count <= 30) {
				srand((unsigned)time(NULL));
				rTime += elapsedTimeInSec;
				if (rTime > re) {
					int temp = AddObject(
						(rand() % 90 - 45) * 0.1f, (rand() % 80 - 40) * 0.1f, 0.f,
						0.5f, 0.5f, 0.5f,
						1, 1, 1, 1,
						0, 0, 0,
						10,
						0.6,
						TYPE_ENEMY,
						20
					);
					rTime = 0.f;
					re -= 0.01f;
				}

				for (int i = 0; i < MAX_OBJECTS; i++) {
					if (m_Object[i] != NULL) {
						m_Object[i]->Update(elapsedTimeInSec);

						int a;
						m_Object[i]->GetType(&a);

						if (a == TYPE_ENEMY) {
							float px, py, pz;
							m_Object[HERO_ID]->GetPos(&px, &py, &pz);
							float x, y, z;
							m_Object[i]->GetPos(&x, &y, &z);
							if (px < x)
								x -= 0.005f;
							else
								x += 0.005f;

							if (py < y)
								y -= 0.005f;
							else
								y += 0.005f;

							m_Object[i]->SetPos(x, y, z);
						}
					}
				}
			}
			else if (count > 30) {
				end = true;
				m_Sound->StopBGSound(g_BGM);
				if (endSong == 0)
					m_Sound->PlayShortSound(g_End, false, 1.f);
				endSong++;
			}

			if (m_Object[HERO_ID] != NULL) {
				float x, y, z;
				m_Object[HERO_ID]->GetPos(&x, &y, &z);
				m_Renderer->SetCameraPos(x, y);
			}
		}
	}
}

void ScnMgr::RenderScene(void) {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0.0f, 0.3f, 0.3f, 1.0f);
	// draw background First
	if (!m_KeyStart) {
		m_Renderer->DrawGround(
			0.f, 0.f, 0.f,
			1000.f, 1000.f, 0.f,
			1, 1, 1, 1,
			g_startTex,
			0.f
		);
	}
	if (end) {
		m_Renderer->DrawGround(
			0.f, 0.f, 0.f,
			1000.f, 1000.f, 0.f,
			1, 1, 1, 1,
			g_endTex,
			0.f
		);
	}
	if (fail) {
		m_Renderer->DrawGround(
			0.f, 0.f, 0.f,
			1000.f, 1000.f, 0.f,
			1, 1, 1, 1,
			g_dieTex,
			0.f
		);
	}
	m_Renderer->DrawGround(
		0.f, 0.f, 0.f,
		1000.f, 1000.f, 0.f,
		1, 1, 1, 1,
		g_testBGTex,
		1.f
	);

	static float pTime = 0.f;
	pTime += 0.016f;

	// Renderer Test
	for (int i = 0; i < MAX_OBJECTS; i++) {
		float x, y, z = 0;
		float sx, sy, sz = 0;
		float r, g, b, a = 0;
		float hp = 0;
		int type = -1;
		if (m_Object[i] != NULL) {
			m_Object[i]->GetPos(&x, &y, &z);
			m_Object[i]->GetVol(&sx, &sy, &sz);
			m_Object[i]->GetColor(&r, &g, &b, &a);
			m_Object[i]->GetHp(&hp);
			m_Object[i]->GetType(&type);
			// 1 meter = 100 cm == 100 pixels
			x = x * 100.f;
			y = y * 100.f;
			z = z * 100.f;
			sx = sx * 100.f;
			sy = sy * 100.f;
			sz = sz * 100.f;

			// m_Renderer->DrawSolidRect(x, y, z, sx, r, g, b, a);

			int texID = -1;
			m_Object[i]->GetTexID(&texID);
			if (type == TYPE_NORMAL) {
				m_Renderer->DrawSolidRectGauge(x, y, z, 0, sy / 2 + 10, 0, sx, 10, 0, 1.f, 0.f, 0.f, 1.f, hp);
				//m_Renderer->SetCameraPos(100.f*x, 100.f*y);
				m_Renderer->DrawTextureRect(x, y, z,
					sx, sy, sz,
					r, g, b, a,
					texID);
			}

			if (type == TYPE_ENEMY) {
				m_Renderer->DrawSolidRectGauge(x, y, z, 0, sy / 2 + 10, 0, sx, 10, 0, 1.f, 0.f, 0.f, 1.f, hp * 5);
				float age = -1;
				m_Object[i]->GetAge(&age);
				age = (int)(age * 4) % 5;
				m_Renderer->DrawTextureRectAnim(x, y, z, sx, sy, sz, 1.f, 1.f, 1.f, 1.f, g_eneTex, 5, 2, (int)age, 0);
				float particleAge;
				m_Object[i]->GetAge(&particleAge);
				m_Renderer->DrawParticle(
					g_Particle,
					x, y, z,
					1,
					1, 1, 1, 1,
					0, 10,
					g_Particle,
					1.f,
					particleAge
				);
			}

			m_Renderer->DrawTextureRect(x, y, z,
				sx, sy, sz,
				r, g, b, a,
				texID);
		}
	}
}

void ScnMgr::DoGarbageCollection()
{
	for (int i = 0; i < MAX_OBJECTS; i++) {
		if (m_Object[i] == NULL)
			continue;

		int type = -1;
		m_Object[i]->GetType(&type);

		if (type == TYPE_BULLET) {
			// Check Velocity Size
			float vx, vy, vz;
			m_Object[i]->GetVel(&vx, &vy, &vz);
			float vSize = sqrtf(vx * vx + vy * vy + vz * vz);
			if (vSize < FLT_EPSILON) {
				DeleteObject(i);
				continue;
			}
		}

		float Hp;
		int a;
		m_Object[i]->GetType(&a);
		m_Object[i]->GetHp(&Hp);
		if (Hp < FLT_EPSILON) {
			DeleteObject(i);
			if (a == TYPE_ENEMY)
				count++;
			if (a == TYPE_NORMAL)
				fail = true;
			continue;
		}
	}
}

int ScnMgr::AddObject(float x, float y, float z,
	float sx, float sy, float sz,
	float r, float g, float b, float a,
	float vx, float vy, float vz,
	float mass,
	float fricCoef,
	int type,
	float Hp)
{
	// Search empty slot
	int idx = -1;
	for (int i = 0; i < MAX_OBJECTS; i++) {
		if (m_Object[i] == NULL) {
			idx = i;
			break;
		}
	}
	if (idx == -1) {
		std::cout << "No more remaining object \n";
		return -1;
	}

	m_Object[idx] = new Object();
	m_Object[idx]->SetPos(x, y, z);
	m_Object[idx]->SetVol(sx, sy, sz);
	m_Object[idx]->SetColor(r, g, b, a);
	m_Object[idx]->SetVel(vx, vy, vz);
	m_Object[idx]->SetMass(mass);
	m_Object[idx]->SetFricCoef(fricCoef);
	m_Object[idx]->SetType(type);
	m_Object[idx]->SetHp(Hp);

	return idx;
}

void ScnMgr::DeleteObject(int idx)
{
	// idx 의 범위 오류 검사 추가해야됨
	if (idx < 0) {
		std::cout << "Negative idx does not allowed. \n";
		return;
	}
	if (idx >= MAX_OBJECTS) {
		std::cout << "Requested idx exceeds MAX_OBJECTS count. \n";
		return;
	}
	if (m_Object[idx] != NULL) {
		delete m_Object[idx];
		m_Object[idx] = NULL;
	}
}

void ScnMgr::KeyDownInput(unsigned char key, int x, int y) {
	if (key == 'w' || key == 'W') {
		m_KeyW = true;
	}
	if (key == 's' || key == 'S') {
		m_KeyS = true;
	}
	if (key == 'a' || key == 'A') {
		m_KeyA = true;
	}
	if (key == 'd' || key == 'D') {
		m_KeyD = true;
	}
	if (key == ' ') {
		m_KeySP = true;
	}
	if (key == '1') {
		m_KeyItem = true;
	}
	if (key == '2') {
		m_KeyStart = true;
	}
}

void ScnMgr::KeyUpInput(unsigned char key, int x, int y) {
	if (key == 'w' || key == 'W') {
		m_KeyW = false;
	}
	if (key == 's' || key == 'S') {
		m_KeyS = false;
	}
	if (key == 'a' || key == 'A') {
		m_KeyA = false;
	}
	if (key == 'd' || key == 'D') {
		m_KeyD = false;
	}
	if (key == ' ') {
		m_KeySP = false;
	}
}

void ScnMgr::SpecialKeyDownInput(int key, int x, int y)
{
	if (key == GLUT_KEY_UP) {
		m_KeyUp = true;
	}
	if (key == GLUT_KEY_DOWN) {
		m_KeyDown = true;
	}
	if (key == GLUT_KEY_LEFT) {
		m_KeyLeft = true;
	}
	if (key == GLUT_KEY_RIGHT) {
		m_KeyRight = true;
	}
}

void ScnMgr::SpecialKeyUpInput(int key, int x, int y)
{
	if (key == GLUT_KEY_UP) {
		m_KeyUp = false;
	}
	if (key == GLUT_KEY_DOWN) {
		m_KeyDown = false;
	}
	if (key == GLUT_KEY_LEFT) {
		m_KeyLeft = false;
	}
	if (key == GLUT_KEY_RIGHT) {
		m_KeyRight = false;
	}
}
