#pragma once
class Object
{
public:
	Object();
	~Object();
	void Initphysics();
	void Update(float elapsedTimeInSec);
	void AddForce(float x, float y, float z, float elapsedTime);
	void SetVol(float x, float y, float z);
	void GetVol(float* x, float* y, float* z);
	void SetPos(float x, float y, float z);
	void GetPos(float* x, float* y, float* z);
	void SetColor(float r, float g, float b, float a);
	void GetColor(float* r, float* g, float* b, float* a);
	void SetVel(float x, float y, float z);
	void GetVel(float* x, float* y, float* z);
	void SetAcc(float x, float y, float z);
	void GetAcc(float* x, float* y, float* z);
	void SetMass(float x);
	void GetMass(float* x);
	void SetType(int type);
	void GetType(int* type);

	void SetFricCoef(float coef);
	void GetFricCoef(float* coef);

	void SetTexID(int id);
	void GetTexID(int* id);

	bool CanShootBullet();
	void ResetBulletCoolTime();

	void SetParentObj(Object* parent);
	Object* GetParentObj();
	bool isAncestor(Object* obj);

	void GetHp(float* hp);
	void SetHp(float hp);
	void Damage(float damage);

	void GetAge(float* a);

private:
	float m_r, m_g, m_b, m_a;
	float m_mass;
	float m_posX, m_posY, m_posZ;
	float m_velX, m_velY, m_velZ;
	float m_accX, m_accY, m_accZ;
	float m_volX, m_volY, m_volZ;
	float m_fricCoef;
	int m_type;														// object type

	Object* m_Parent = NULL;

	float m_remainingCoolTime = 0.f;
	float m_CurrentCoolTime = 0.1f;

	int m_texID;		// texID
	float HealthPoint;
	float age = 0;
};

